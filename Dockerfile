
FROM openjdk:8-jre-alpine

ARG ARCHIVE=archive/apache-activemq-5.16.0-bin.tar.gz
ARG CONFIG=config/jetty.xml

COPY ${ARCHIVE} .
COPY ${CONFIG} .

RUN tar -xzf apache-activemq-5.16.0-bin.tar.gz

RUN echo "y" | rm apache-activemq-5.16.0/conf/jetty.xml &&\
    cp jetty.xml apache-activemq-5.16.0/conf

CMD ["apache-activemq-5.16.0/bin/activemq", "console"]

# local build
# docker build -t activemq-docker-build .
# && local run on localhost
# docker run --net=host activemq-docker-build